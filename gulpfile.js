const gulp = require('gulp');
const imagemin = require("gulp-imagemin");
const uglify = require("gulp-uglify");
const sass = require('gulp-sass')(require('sass'));
const concat = require('gulp-concat');
const browserSync = require("browser-sync").create();
const clean = require("gulp-clean");
const autoprefixer = require("gulp-autoprefixer");
const cleanCSS = require("gulp-clean-css");
const rename = require("gulp-rename")
const gulpHtml = require("gulp-html-partial")
const fileinclude = require("gulp-file-include")

// gulp.task('message', function (done) {
// 	console.log("hello world");
// 	done();
// });

function cleanDist() {
	return gulp.src("./dist/*", {read: false}).pipe(clean());
}

// <<<<<<<<<<<Copy html files>>>>>>>>>>>>

gulp.task('html', function (done) {
	return gulp.src("./src/index.html")
		.pipe(fileinclude({
			basePath: 'src/html/'
		}))
		.pipe(gulp.dest("dist"))
		.on("end", done);
})
// gulp.task('html', function (done) {
// 	return gulp.src("./src/index.html")
// 		.pipe(gulpHtml({
// 			basePath: 'src/html/'
// 		}))
// 		.pipe(gulp.dest("dist/"))
// 		.on("end", done);
// })
// <<<<<<<<<<<<Minify images>>>>>>>>>>>
gulp.task("images", function (done) {
	gulp
		.src("./src/img/**/*")
		.pipe(imagemin())
		.pipe(gulp.dest("./dist/img"))
		.on("end", done);
})
// <<<<<<<<<<<Minify and Concatenate js>>>>>>>>>>>
gulp.task("js", function (done) {
	gulp
		.src("./src/js/**/*.js")
		.pipe(uglify())
		.pipe(concat("scripts.min.js"))
		.pipe(gulp.dest("./dist/js"))
		.on("end", done);
})
// <<<<<<<<Compile sass>>>>>>>>>>
gulp.task("sass", function (done) {
	gulp
		.src("./src/scss/style.scss")
		.pipe(sass().on("error", sass.logError))
		.pipe(
			autoprefixer(["last 15 versions", "> 1%", "ie 8", "ie 7"], {
				cascade: true,
			})
		)
		.pipe(cleanCSS({compatibility: "ie8"}))
		.pipe(rename("styles.min.css"))
		// .pipe(concat("styles.min.css"))
		.pipe(gulp.dest("dist/css"))
		.on("end", done);
})

// <<<<<<<Watch files for changes>>>>>>>>>>
gulp.task("watch", function () {
	gulp
		.watch("./src/**/*.html", gulp.series("html"))
		.on("change", browserSync.reload)
	gulp
		.watch("./src/img/**/*", gulp.series("images"))
		.on("change", browserSync.reload)
	gulp
		.watch("./src/js/**/*.js", gulp.series("js"))
		.on("change", browserSync.reload)
	gulp
		.watch("./src/scss/**/*.scss", gulp.series("sass"))
		.on("change", browserSync.reload)
})
gulp.task("serve", function () {
	browserSync.init({
		server: {
			baseDir: ["dist"],
		},
		port: 9000,
		open: true,
	});
});


gulp.task("dev", gulp.parallel("serve", "watch"));
gulp.task("build", gulp.series(cleanDist, gulp.parallel("html", "sass", "js", "images")));
