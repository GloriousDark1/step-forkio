## Список використаних технологій

### Збірка проекту

* gulp;  
* gulp-sass;
* browser-sync;
* gulp-autoprefixer;
* gulp-clean;
* gulp-clean-css;
* gulp-concat;
* gulp-imagemin;
* gulp-rename;
* gulp-uglify;
* sass;
* gulp-file-include

###  Написання проекту

* Grid;
* Flex;
* Position;
* Медіазапроси;
* Змінні;
* Hover;
* Focus;
* Normalize;

## Склад учасників проєкту

1. Студент №1 - Миколенко Вадим
2. Студент №2 - Елисеев Антон

## Виконані завдання

1. Студент №1 - Миколенко Вадим

* Створив репозиторій;
* Зробив збірку для проєкту;
* Зверстав блок Revolutionary Editor;
* Зверстав секцію Here is what you get;
* Зверстав секцію Fork Subscription Pricing;

2. Студент №2 - Елисеев Антон

* Зверстав шапку сайту з верхнім меню (включаючи випадаюче меню при малій роздільній здатності екрана.);
* Зверстав секцію People Are Talking About Fork;

